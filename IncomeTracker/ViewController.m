//
//  ViewController.m
//  IncomeTracker
//
//  Created by jpeger on 2/8/15.
//  Copyright (c) 2015 jpeger. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    MainNavigationViewController* nav = [MainNavigationViewController new];
    
    UINavigationController* navCon = [[UINavigationController alloc]initWithRootViewController:nav];
    
    
    
    [self.view addSubview:navCon.view];
    [self addChildViewController:navCon];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
