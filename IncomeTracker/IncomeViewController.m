//
//  IncomeViewController.m
//  IncomeTracker
//
//  Created by jpeger on 2/8/15.
//  Copyright (c) 2015 jpeger. All rights reserved.
//

#import "IncomeViewController.h"
#import "IncomeTableViewCell.h"

@interface IncomeViewController ()

@end

@implementation IncomeViewController

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    
    
    if (self) {
        self.title = @"Income";
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    incomeTable = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
    [incomeTable setDataSource:self];
    [incomeTable setDelegate:self];
    
    
    
    [self.view addSubview:incomeTable];
    [self registerForNotifications];
    if (!self.incomeItems) {
        self.incomeItems = [[NSMutableArray alloc]init];
    }
    // Do any additional setup after loading the view.
}

-(void)registerForNotifications {
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(incomeItem:) name:@"incomeAdded" object:nil];
}

-(void)incomeItem:(NSNotification*)n {
    NSLog(@"%@", n.object);
    [self.incomeItems addObject:n.object];
    [incomeTable reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    IncomeTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[IncomeTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    NSMutableDictionary* temp = [self.incomeItems objectAtIndex:indexPath.row];
    NSNumber* temp2 =[temp objectForKey:@"trackerAmount"];
    [cell setIncomeName:[temp objectForKey:@"trackerTitle"]];
    [cell setIncomeAmount:temp2.doubleValue];
    
    return cell;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"editTracker" object:passThrough];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self loadData];
}

-(void)loadData{
    AppDelegate* ad = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    self.incomeItems = ad.income;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.incomeItems.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [incomeTable deselectRowAtIndexPath:indexPath animated:YES];
    addItemViewController* addItemView = [addItemViewController new];
    
    passThrough = [self.incomeItems objectAtIndex:indexPath.row];
    
    
    [self.incomeItems removeObjectAtIndex:indexPath.row];
    
    [self.navigationController pushViewController:addItemView animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
