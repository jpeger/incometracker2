//
//  IncomeTableViewCell.h
//  IncomeTracker
//
//  Created by jpeger on 2/8/15.
//  Copyright (c) 2015 jpeger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IncomeTableViewCell : UITableViewCell

{
    UILabel* incomeName;
    UILabel* incomeAmount;
}

-(void)setIncomeName:(NSString*)name;

-(void)setIncomeAmount:(double)amount;

@end
