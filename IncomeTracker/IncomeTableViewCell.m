//
//  IncomeTableViewCell.m
//  IncomeTracker
//
//  Created by jpeger on 2/8/15.
//  Copyright (c) 2015 jpeger. All rights reserved.
//

#import "IncomeTableViewCell.h"

@implementation IncomeTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self){
        incomeName = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, self.frame.size.width/2 - 10, self.frame.size.height-20)];
        incomeAmount = [[UILabel alloc]initWithFrame:CGRectMake(self.frame.size.width/2, 10, self.frame.size.width/2 - 10, self.frame.size.height - 20)];
        
        [incomeAmount setTextAlignment:NSTextAlignmentRight];
        
        [self addSubview:incomeName];
        [self addSubview:incomeAmount];
    }
    
    return self;
}

-(void)setIncomeName:(NSString *)name{
    [incomeName setText:name];
}

-(void)setIncomeAmount:(double)amount{
    NSNumberFormatter* nsnf = [[NSNumberFormatter alloc]init];
    [nsnf setNumberStyle:NSNumberFormatterCurrencyStyle];
    [incomeAmount setText:[nsnf stringFromNumber:[NSNumber numberWithDouble:amount]]];
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
