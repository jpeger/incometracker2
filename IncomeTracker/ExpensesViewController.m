//
//  ExpensesViewController.m
//  IncomeTracker
//
//  Created by jpeger on 2/8/15.
//  Copyright (c) 2015 jpeger. All rights reserved.
//

#import "ExpensesViewController.h"
#import "IncomeTableViewCell.h"

@interface ExpensesViewController ()

@end

@implementation ExpensesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    
    
    
    if (self) {
        //set the title for the tab
        self.title = @"Expenses";
        //set the image icon for the tab
        
        //        self.tabBarItem.image = [UIImage imageWithIcon:@"fa-calendar" backgroundColor:[UIColor clearColor] iconColor:[UIColor whiteColor] andSize:CGSizeMake(30, 30)];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!self.expensesItems) {
        self.expensesItems = [[NSMutableArray alloc]init];
    }
    
    expensesTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];
    [expensesTable setDelegate:self];
    [expensesTable setDataSource:self];
    
    [self.view addSubview:expensesTable];
    [self registerForNotifications];
    [expensesTable reloadData];
    // Do any additional setup after loading the view.
}

-(void)registerForNotifications {
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(incomeItem:) name:@"expenseAdded" object:nil];
}

-(void)incomeItem:(NSNotification*)n {
    NSLog(@"%@", n.object);
    [self.expensesItems addObject:n.object];
    [expensesTable reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    IncomeTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[IncomeTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    NSMutableDictionary* temp = [self.expensesItems objectAtIndex:indexPath.row];
    NSNumber* temp2 =[temp objectForKey:@"trackerAmount"];
    [cell setIncomeName:[temp objectForKey:@"trackerTitle"]];
    [cell setIncomeAmount:temp2.doubleValue];
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.expensesItems.count;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self loadData];
}

-(void)loadData{
    AppDelegate* ad = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    self.expensesItems = ad.expenses;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"editTracker" object:passThrough];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [expensesTable deselectRowAtIndexPath:indexPath animated:YES];
    
    
    addItemViewController* addItemView = [addItemViewController new];
    
    passThrough = [self.expensesItems objectAtIndex:indexPath.row];
    
    
    [self.expensesItems removeObjectAtIndex:indexPath.row];
    
    [self.navigationController pushViewController:addItemView animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
