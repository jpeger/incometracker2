//
//  addItemViewController.h
//  IncomeTracker
//
//  Created by jpeger on 2/16/15.
//  Copyright (c) 2015 jpeger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface addItemViewController : UIViewController<UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate,MFMailComposeViewControllerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    NSArray* arrayOfTypes;
    NSMutableDictionary* trackerDictionary;    UIImageView* recipt;
    UITextField* amount;
    UITextField* title;
    UIDatePicker* datePicker;
    UIPickerView* trackerType;
    NSString* trackerTypeString;
}


@end
