//
//  IncomeViewController.h
//  IncomeTracker
//
//  Created by jpeger on 2/8/15.
//  Copyright (c) 2015 jpeger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "addItemViewController.h"
#import "AppDelegate.h"

@interface IncomeViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

{
    UITableView* incomeTable;
    NSMutableDictionary* passThrough;
}

@property (strong, nonatomic) NSMutableArray* incomeItems;

@end
