//
//  MainNavigationViewController.m
//  IncomeTracker
//
//  Created by jpeger on 2/16/15.
//  Copyright (c) 2015 jpeger. All rights reserved.
//

#import "MainNavigationViewController.h"

@interface MainNavigationViewController ()

@end

@implementation MainNavigationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    IncomeViewController* vic = [IncomeViewController new];
    ExpensesViewController* vec = [ExpensesViewController new];
    MainTabBarViewController* mTab = [MainTabBarViewController new];
    
    NSArray* vcsArray = @[
                          vic,
                          vec
                          ];
    [mTab setViewControllers:vcsArray];

    [self.view addSubview:mTab.view];
    [self addChildViewController:mTab];
    
    UIBarButtonItem* addItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addTrackerItem)];
    
    self.navigationItem.rightBarButtonItem = addItem;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addTrackerItem{
    addItemViewController* addItems = [[addItemViewController alloc]init];
    
    [self.navigationController pushViewController:addItems animated:YES];
    
    //[self.navigationController presentViewController:addItems animated:YES completion:^{
        
    //}];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
