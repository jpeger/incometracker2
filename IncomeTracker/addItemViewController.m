//
//  addItemViewController.m
//  IncomeTracker
//
//  Created by jpeger on 2/16/15.
//  Copyright (c) 2015 jpeger. All rights reserved.
//

#import "addItemViewController.h"

@interface addItemViewController ()

@end

@implementation addItemViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    trackerTypeString = @"Income";
//    if (trackerDictionary == nil) {
//        trackerDictionary = [[NSMutableDictionary alloc]init];
//    }else{
//        [amount setText:trackerDictionary[@"trackerAmount"]];
//        [title setText:trackerDictionary[@"trackerTitle"]];
//        [recipt setImage:[self getMyImage:trackerDictionary[@"imagePath"]]];
//        [datePicker setDate:trackerDictionary[@"trackerDate"]];
//    }
    
    arrayOfTypes = @[@"Income",@"Expense"];
    
    trackerType = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 20, self.view.frame.size.width, 150)];
    [trackerType setDataSource:self];
    [trackerType setDelegate:self];
    
    [trackerType setBackgroundColor:[UIColor whiteColor]];
    
    datePicker = [[UIDatePicker alloc]initWithFrame:CGRectMake(0, trackerType.frame.size.height-40, self.view.frame.size.height, 150)];
    datePicker.datePickerMode = UIDatePickerModeDate;
    
    title = [[UITextField alloc]initWithFrame:CGRectMake(10, datePicker.frame.size.height+datePicker.frame.origin.y-60, self.view.frame.size.width-20, 40)];
    [title setBackgroundColor:[UIColor whiteColor]];
    [title setBorderStyle:UITextBorderStyleLine];
    [title setDelegate:self];
    title.placeholder = @"Title";
    
    amount = [[UITextField alloc]initWithFrame:CGRectMake(10, title.frame.size.height+title.frame.origin.y, self.view.frame.size.width-20, 40)];
    [amount setBackgroundColor:[UIColor whiteColor]];
    [amount setBorderStyle:UITextBorderStyleLine];
    [amount setDelegate:self];
    [amount setPlaceholder:@"amount"];
    [amount setKeyboardType:UIKeyboardTypeDecimalPad];
    
    
    UIButton* btn = [UIButton buttonWithType:UIButtonTypeSystem];
    [btn setTitle:@"Done Editing" forState:UIControlStateNormal];
    [btn setFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    [btn addTarget:self action:@selector(doneEditing) forControlEvents:UIControlEventTouchUpInside];
    
    [title setInputAccessoryView:btn];
    [amount setInputAccessoryView:btn];
    
    recipt = [[UIImageView alloc]initWithFrame:CGRectMake(10, amount.frame.size.height+amount.frame.origin.y+5, self.view.frame.size.width-20, 300)];
    
    UIBarButtonItem* save = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveTracker)];
    UIBarButtonItem* takePick = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCamera target:self action:@selector(takePhoto:)];
    UIBarButtonItem* loadPick = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemOrganize target:self action:@selector(selectPhoto:)];
    UIBarButtonItem* composeEmail = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCompose target:self action:@selector(composeLetter)];
    
    NSArray* btnItems = [[NSArray alloc]initWithObjects:save, takePick, loadPick, composeEmail, nil];
    
    self.navigationItem.rightBarButtonItems = btnItems;
    
    [self.view addSubview:datePicker];
    [self.view addSubview:trackerType];
    [self.view addSubview:title];
    [self.view addSubview:amount];
    [self.view addSubview:recipt];
    [self registerForNotifications];
}

-(void)registerForNotifications {
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(editTracker:) name:@"editTracker" object:nil];
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    NSLog(@"Dictionary at will appear %@",trackerDictionary);
    if (trackerDictionary == nil) {
        trackerDictionary = [[NSMutableDictionary alloc]init];
    }
}



-(void)editTracker:(NSNotification*)n{
    trackerDictionary = n.object;
    if (trackerDictionary) {
        [amount setText:[NSString stringWithFormat:@"%@",trackerDictionary[@"trackerAmount"]]];
        [title setText:trackerDictionary[@"trackerTitle"]];
        [recipt setImage:[self getMyImage:trackerDictionary[@"imagePath"]]];
        [datePicker setDate:trackerDictionary[@"trackerDate"]];
    }
}

-(void)composeLetter{
    MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
    controller.mailComposeDelegate = self;
    [controller setSubject:title.text];
    [controller setToRecipients:@[@"stephenbromley@mail.weber.edu"]];
    [controller setMessageBody:amount.text isHTML:NO];
    if (controller) [self presentViewController:controller animated:YES completion:^{
        
    }];
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    switch (component) {
        case 0:
            return arrayOfTypes[row];
            break;
            
        default:
            return @"Nothing";
            break;
    }
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    trackerTypeString = arrayOfTypes[row];
}



-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return arrayOfTypes.count;
}

-(void)doneEditing{
    [self.view endEditing:YES];
}

-(void)saveTracker{
    NSNumber* amt = [[NSNumber alloc]initWithDouble:[amount.text doubleValue]];
    [trackerDictionary setObject:amt forKey:@"trackerAmount"];
    
    [trackerDictionary setObject:title.text forKey:@"trackerTitle"];
    [trackerDictionary setObject:trackerTypeString forKey:@"trackerType"];
    [trackerDictionary setObject:datePicker.date forKey:@"trackerDate"];
    
    if ([trackerTypeString  isEqual: @"Income"]) {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"incomeAdded" object:trackerDictionary];
    } else {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"expenseAdded" object:trackerDictionary];
    }
    
    [self.navigationController popToRootViewControllerAnimated:YES];

}

- (void)takePhoto:(UIButton *)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    //    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:^{
        NSLog(@"Picker presented for camera");
    }];
    
}


- (void)selectPhoto:(UIButton *)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    //    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:^{
        NSLog(@"Picker presented for Photo library");
    }];
    
    
}


#pragma mark - Image Picker Controller Stuff

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    //    self.imageView.image = chosenImage;
    //    iv.image = chosenImage;
    
    NSDate* currentTime = [NSDate date];
    NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
    
    [dateFormater setDateFormat:@"yyyy-MM-DD HH:mm:ss"];
    NSString *convertedDateString = [dateFormater stringFromDate:currentTime];
    
    
    
    NSString* savedPath = [self saveMyImage:chosenImage withTimeString:convertedDateString];
    
    recipt.image = [self getMyImage:savedPath];
    [trackerDictionary setObject:savedPath forKey:@"imagePath"];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        
        
        
        
    }];
    
}

-(UIImage*)getMyImage:(NSString*)pathString {
    
    return [UIImage imageWithContentsOfFile:pathString];
}

-(NSString*)saveMyImage:(UIImage*)myImage withTimeString:(NSString*)timeString {
    NSData *imageData = UIImagePNGRepresentation(myImage);
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *imagePath =[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",timeString]];
    
    NSLog((@"pre writing to file"));
    if (![imageData writeToFile:imagePath atomically:NO])
    {
        NSLog((@"Failed to cache image data to disk"));
        return nil;
    }
    else
    {
        NSLog(@"the cachedImagedPath is %@",imagePath);
        return imagePath;
    }
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
