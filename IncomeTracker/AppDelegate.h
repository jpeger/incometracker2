//
//  AppDelegate.h
//  IncomeTracker
//
//  Created by jpeger on 2/8/15.
//  Copyright (c) 2015 jpeger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    NSUserDefaults* defaults;
}
@property (strong, nonatomic) UIWindow *window;

@property(strong, nonatomic) NSMutableArray* income;
@property(strong, nonatomic) NSMutableArray* expenses;

@end

